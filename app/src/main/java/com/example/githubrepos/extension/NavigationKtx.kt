package com.example.githubrepos.extension

import androidx.annotation.IdRes
import androidx.fragment.app.Fragment
import androidx.navigation.NavDirections
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController

fun Fragment.navigateSafeWithDirections(
    @IdRes currentDestination: Int,
    navDirections: NavDirections,
    navOptions: NavOptions? = null
) {
    if (findNavController().currentDestination?.id == currentDestination) {
        findNavController().navigate(navDirections, navOptions)
    }
}