package com.example.githubrepos.ui.details

import android.os.Bundle
import android.view.View
import androidx.activity.OnBackPressedCallback
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.githubrepos.R
import com.example.githubrepos.base.BaseFragment
import com.example.githubrepos.databinding.FragmentRepositoryDetailsBinding
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class RepositoryDetailsFragment : BaseFragment<FragmentRepositoryDetailsBinding>() {

    override val layoutResId: Int = R.layout.fragment_repository_details
    private val viewModel by viewModel<RepositoryDetailsViewModel>()

    private val args: RepositoryDetailsFragmentArgs by navArgs()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.viewModel = viewModel
        viewModel.onRepoDetailsAvailable(args.repoDetails)
        Timber.d("${args.repoDetails}")
    }

}