package com.example.githubrepos.ui.details

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.githubrepos.data.model.domain.RepositoryDetails
import com.example.githubrepos.extension.asLiveData

class RepositoryDetailsViewModel : ViewModel() {
    private val _repoDetails = MutableLiveData<RepositoryDetails>()
    val repoDetails = _repoDetails.asLiveData()

    fun onRepoDetailsAvailable(repoDetails: RepositoryDetails) {
        _repoDetails.value = repoDetails
    }
}