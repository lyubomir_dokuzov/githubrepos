package com.example.githubrepos.ui.repositories

import androidx.lifecycle.MutableLiveData
import com.example.githubrepos.data.model.domain.RepositoryDetails
import com.example.githubrepos.extension.asLiveData
import com.example.githubrepos.misc.ItemClickedListener

class GitHubRepositoryListItemViewModel(private val selected: ItemClickedListener<RepositoryDetails>) {
    private val _repository = MutableLiveData<RepositoryDetails>()
    val repository = _repository.asLiveData()

    fun bind(repo: RepositoryDetails?) {
        _repository.value = repo
    }

    fun onItemSelected() {
        _repository.value?.let {
            selected.onItemClicked(it)
        }
    }
}