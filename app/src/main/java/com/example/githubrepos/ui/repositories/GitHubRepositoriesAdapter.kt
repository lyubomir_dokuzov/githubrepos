package com.example.githubrepos.ui.repositories

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.githubrepos.data.model.domain.RepositoryDetails
import com.example.githubrepos.databinding.LayoutRepositoryListItemBinding
import com.example.githubrepos.misc.ItemClickedListener

class GitHubRepositoriesAdapter(
    private val viewLifecycleOwner: LifecycleOwner,
    private val selected: ItemClickedListener<RepositoryDetails>
) : ListAdapter<RepositoryDetails, GitHubRepositoriesAdapter.GitHubRepositoryViewHolder>(
    DIFF_CALLBACK
) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GitHubRepositoryViewHolder {
        val binding = LayoutRepositoryListItemBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return GitHubRepositoryViewHolder(binding, viewLifecycleOwner, selected)
    }

    override fun onBindViewHolder(holder: GitHubRepositoryViewHolder, position: Int) {
        holder.onBind(getItem(position))
    }

    class GitHubRepositoryViewHolder(
        private val binding: LayoutRepositoryListItemBinding,
        viewLifecycleOwner: LifecycleOwner,
        selected: ItemClickedListener<RepositoryDetails>
    ) : RecyclerView.ViewHolder(binding.root) {

        private val viewModel = GitHubRepositoryListItemViewModel(selected)

        init {
            binding.viewModel = viewModel
            binding.lifecycleOwner = viewLifecycleOwner
        }

        fun onBind(repo: RepositoryDetails?) {
            viewModel.bind(repo)
            binding.executePendingBindings()
        }
    }

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<RepositoryDetails>() {
            override fun areItemsTheSame(
                oldItem: RepositoryDetails,
                newItem: RepositoryDetails
            ) = oldItem.fullName == newItem.fullName

            override fun areContentsTheSame(
                oldItem: RepositoryDetails,
                newItem: RepositoryDetails
            ) = oldItem == newItem
        }
    }
}