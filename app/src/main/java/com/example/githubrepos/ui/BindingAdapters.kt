package com.example.githubrepos.ui

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.squareup.picasso.Picasso

@BindingAdapter("app:imageUrl")
fun loadImage(view: ImageView?, imageUrl: String?) {
    imageUrl?.let {
        Picasso.get().load(imageUrl).into(view)
    }
}