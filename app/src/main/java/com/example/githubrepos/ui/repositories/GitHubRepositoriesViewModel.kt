package com.example.githubrepos.ui.repositories

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.githubrepos.data.model.domain.RepositoryDetails
import com.example.githubrepos.data.repository.GitHubRepositoriesRepository
import com.example.githubrepos.extension.asLiveData
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onEach
import timber.log.Timber

class GitHubRepositoriesViewModel(
    private val gitHubRepositoriesRepository: GitHubRepositoriesRepository
) : ViewModel() {

    private var currentPage = 1
    private var loading = false
    private var _results = MutableLiveData<List<RepositoryDetails>>()
    val results = _results.asLiveData()

    fun getRepositories() {
        currentPage = 1
        loadItems()
    }

    fun loadMore() {
        if (!loading) {
            currentPage = currentPage.plus(1)
            loadItems()
        }
    }

    private fun loadItems() {
        loading = true
        gitHubRepositoriesRepository.getRepositories(buildQuery(currentPage))
            .onEach {
                _results.value = it
            }
            .catch { cause ->
                // Error handling
                Timber.e(cause)
            }
            .onCompletion {
                loading = false
            }
            .launchIn(viewModelScope)
    }

    private fun buildQuery(pageIndex: Int): MutableMap<String, String> =
        mutableMapOf<String, String>().apply {
            put("q", "language:$SEARCH_LANG")
            put("sort", "stars")
            put("order", "desc")
            put("page", "$pageIndex")
            put("per_page", "$PER_PAGE")
        }

    companion object {
        private const val SEARCH_LANG = "kotlin"
        private const val PER_PAGE = 20
    }
}