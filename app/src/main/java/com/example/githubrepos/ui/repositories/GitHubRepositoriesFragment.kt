package com.example.githubrepos.ui.repositories

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.githubrepos.R
import com.example.githubrepos.base.BaseFragment
import com.example.githubrepos.data.model.domain.RepositoryDetails
import com.example.githubrepos.databinding.FragmentRepositoriesBinding
import com.example.githubrepos.extension.navigateSafeWithDirections
import com.example.githubrepos.misc.ItemClickedListener
import com.example.githubrepos.misc.RecyclerEndReached
import com.example.githubrepos.misc.RecyclerScrollListener
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class GitHubRepositoriesFragment : BaseFragment<FragmentRepositoriesBinding>(),
    ItemClickedListener<RepositoryDetails>, RecyclerEndReached {

    override val layoutResId: Int = R.layout.fragment_repositories
    private val viewModel by viewModel<GitHubRepositoriesViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.getRepositories()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val adapter = GitHubRepositoriesAdapter(viewLifecycleOwner, this)

        binding.rvRepositories.layoutManager = LinearLayoutManager(context)
        binding.rvRepositories.adapter = adapter
        binding.rvRepositories.addOnScrollListener(RecyclerScrollListener(this))

        viewModel.results.observe(viewLifecycleOwner) {
            adapter.submitList(it)
            adapter.notifyDataSetChanged()
        }
    }

    override fun onItemClicked(item: RepositoryDetails) {
        navigateSafeWithDirections(
            R.id.repositoryList,
            GitHubRepositoriesFragmentDirections.actionNavigateToDetails(item)
        )
    }

    override fun onEndReached() {
        viewModel.loadMore()
    }
}
