package com.example.githubrepos

import android.app.Application
import com.example.githubrepos.di.modules
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import timber.log.Timber

class GRApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())

        startKoin {
            androidContext(this@GRApplication)
            modules(modules)
        }
    }
}