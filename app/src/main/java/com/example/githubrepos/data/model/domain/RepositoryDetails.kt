package com.example.githubrepos.data.model.domain

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class RepositoryDetails(
    val fullName: String,
    val stars: String,
    val language: String,
    val avatarUrl: String,
    val lastUpdate: String
) : Parcelable