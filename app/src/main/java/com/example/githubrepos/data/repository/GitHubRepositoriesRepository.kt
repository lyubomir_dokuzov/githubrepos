package com.example.githubrepos.data.repository

import com.example.githubrepos.data.GitHubApi
import com.example.githubrepos.data.model.domain.RepositoryDetails
import com.example.githubrepos.data.model.mapping.GitHubRepositoryMapper
import com.example.githubrepos.misc.Dispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import timber.log.Timber
import java.lang.RuntimeException

interface GitHubRepositoriesRepository {
    fun getRepositories(params: Map<String, String>): Flow<List<RepositoryDetails>>
}

class GitHubRepositoriesRepositoryImpl(
    private val gitHubApi: GitHubApi,
    private val mapper: GitHubRepositoryMapper,
    private val dispatcher: Dispatcher
) : GitHubRepositoriesRepository {

    private val results = mutableListOf<RepositoryDetails>()

    override fun getRepositories(params: Map<String, String>): Flow<List<RepositoryDetails>> =
        flow {
            gitHubApi.getRepositories(params).body()?.let { body ->
                results.addAll(body.items.map { mapper.map(it) })
                emit(results)
            }
        }.flowOn(dispatcher.io())
}