package com.example.githubrepos.data.model.remote

import com.fasterxml.jackson.annotation.JsonProperty
import java.util.*

class RetGithubRepositoriesResponse(
    @JsonProperty("total_count")
    val total: Int,
    val items: List<GitHubRepository>
)

data class GitHubRepository(
    val name: String,
    val owner: Owner,
    @JsonProperty("updated_at")
    val updatedAt: Date,
    val language: String,
    @JsonProperty("stargazers_count")
    val stars: Long
)

data class Owner(
    val login: String,
    @JsonProperty("avatar_url")
    val avatarUrl: String
)


