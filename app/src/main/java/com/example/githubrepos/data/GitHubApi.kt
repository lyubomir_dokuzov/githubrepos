package com.example.githubrepos.data

import com.example.githubrepos.data.model.remote.RetGithubRepositoriesResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.QueryMap

interface GitHubApi {
    @GET("search/repositories")
    suspend fun getRepositories(@QueryMap query: Map<String, String>): Response<RetGithubRepositoriesResponse>
}