package com.example.githubrepos.data.model.mapping

interface DomainMapper<T, R> {
    suspend fun map(input: T): R
}