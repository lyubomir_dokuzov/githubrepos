package com.example.githubrepos.data.model.mapping

import com.example.githubrepos.data.model.domain.RepositoryDetails
import com.example.githubrepos.data.model.remote.GitHubRepository
import com.example.githubrepos.misc.Dispatcher
import kotlinx.coroutines.withContext
import java.text.SimpleDateFormat
import java.util.*

class GitHubRepositoryMapper(
    private val dispatcher: Dispatcher
) : DomainMapper<GitHubRepository, RepositoryDetails> {
    private val sdf = SimpleDateFormat(DATE_FORMAT, Locale.getDefault())

    override suspend fun map(input: GitHubRepository): RepositoryDetails =
        withContext(dispatcher.io()) {
            RepositoryDetails(
                fullName = input.owner.login + "/" + input.name,
                stars = "Stars: ${input.stars}",
                language = "Language: ${input.language}",
                avatarUrl = input.owner.avatarUrl,
                lastUpdate = "Last updated: ${sdf.format(input.updatedAt)}"
            )
        }

    companion object {
        private const val DATE_FORMAT = "dd MMM yyyy"
    }
}