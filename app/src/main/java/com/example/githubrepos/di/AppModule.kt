package com.example.githubrepos.di

import com.example.githubrepos.data.model.mapping.GitHubRepositoryMapper
import com.example.githubrepos.misc.Dispatcher
import com.example.githubrepos.misc.Dispatcherimpl
import org.koin.dsl.module

val appModule = module {
    single<Dispatcher> { Dispatcherimpl() }
    single { GitHubRepositoryMapper(get()) }
}