package com.example.githubrepos.di

import com.example.githubrepos.R
import com.example.githubrepos.data.GitHubApi
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.moczul.ok2curl.CurlInterceptor
import okhttp3.OkHttpClient
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory
import timber.log.Timber

val networkModule = module {
    single {
        jacksonObjectMapper().apply {
            configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
            configure(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_USING_DEFAULT_VALUE, true)
        }
    }

    single {
        JacksonConverterFactory.create(get())
    }

    single {
        OkHttpClient.Builder()
            .addInterceptor(CurlInterceptor { message -> Timber.d(message) })
            .build()
    }


    single {
        Retrofit.Builder()
            .baseUrl(androidContext().getString(R.string.base_url))
            .client(get())
            .addConverterFactory(get<JacksonConverterFactory>())
            .build()
    }

    single {
        get<Retrofit>().create(GitHubApi::class.java)
    }

}
