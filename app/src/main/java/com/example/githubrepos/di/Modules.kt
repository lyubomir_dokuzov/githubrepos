package com.example.githubrepos.di

val modules = listOf(
    viewModelModule, networkModule, appModule, repositories
)
