package com.example.githubrepos.di

import com.example.githubrepos.ui.details.RepositoryDetailsViewModel
import com.example.githubrepos.ui.repositories.GitHubRepositoriesViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { GitHubRepositoriesViewModel(get()) }
    viewModel { RepositoryDetailsViewModel() }
}