package com.example.githubrepos.di

import com.example.githubrepos.data.repository.GitHubRepositoriesRepository
import com.example.githubrepos.data.repository.GitHubRepositoriesRepositoryImpl
import org.koin.dsl.module

val repositories = module {
    factory <GitHubRepositoriesRepository> { GitHubRepositoriesRepositoryImpl(get(), get(), get()) }
}