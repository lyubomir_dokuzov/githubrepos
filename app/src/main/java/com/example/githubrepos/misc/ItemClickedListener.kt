package com.example.githubrepos.misc

interface ItemClickedListener<T> {
    fun onItemClicked(item: T)
}