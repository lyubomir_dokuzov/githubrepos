package com.example.githubrepos.misc

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class RecyclerScrollListener(private val recyclerEndReached: RecyclerEndReached) :
    RecyclerView.OnScrollListener() {
    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        val layoutManager = recyclerView.layoutManager as LinearLayoutManager
        if (dy > 0 &&
            (layoutManager.itemCount - layoutManager.findLastVisibleItemPosition()) <= ITEMS_BEFORE_END
        ) {
            recyclerEndReached.onEndReached()
        }
    }

    companion object {
        private const val ITEMS_BEFORE_END = 5
    }
}