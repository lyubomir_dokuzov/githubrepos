package com.example.githubrepos.misc

interface RecyclerEndReached {
    fun onEndReached()
}