package com.example.githubrepos.ui.repositories

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.githubrepos.data.model.domain.RepositoryDetails
import com.example.githubrepos.misc.ItemClickedListener
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.mockk
import io.mockk.verify
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class GitHubRepositoryListItemViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @MockK
    lateinit var clickListener: ItemClickedListener<RepositoryDetails>

    lateinit var viewModel: GitHubRepositoryListItemViewModel

    @Before
    fun setup() {
        MockKAnnotations.init(this)
        every { clickListener.onItemClicked(any()) } returns Unit
        viewModel = GitHubRepositoryListItemViewModel(clickListener)
    }

    @Test
    fun `bound item set correctly`() {
        val mockRepo = getMockedRepoDetails()
        viewModel.bind(mockRepo)
        assertEquals(mockRepo, viewModel.repository.value)
    }

    @Test
    fun `on item clicked propagated`() {
        val mockRepo = getMockedRepoDetails()
        viewModel.bind(mockRepo)
        viewModel.onItemSelected()

        verify {
            clickListener.onItemClicked(eq(mockRepo))
        }
    }

    private fun getMockedRepoDetails() = mockk<RepositoryDetails> {
        every { fullName } returns "Full name"
        every { stars } returns "Stars: 55"
        every { language } returns "kotlin"
        every { avatarUrl } returns "some/url/filename.png"
        every { lastUpdate } returns "10 oct 2020"
    }

}