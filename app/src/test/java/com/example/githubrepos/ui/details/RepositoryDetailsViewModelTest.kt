package com.example.githubrepos.ui.details

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.githubrepos.data.model.domain.RepositoryDetails
import io.mockk.every
import io.mockk.mockk
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class RepositoryDetailsViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    lateinit var viewModel: RepositoryDetailsViewModel

    @Before
    fun setup() {
        viewModel = RepositoryDetailsViewModel()
    }

    @Test
    fun `bound item set correctly`() {
        val mockRepo = getMockedRepoDetails()
        viewModel.onRepoDetailsAvailable(mockRepo)
        assertEquals(mockRepo, viewModel.repoDetails.value)
    }

    private fun getMockedRepoDetails() = mockk<RepositoryDetails> {
        every { fullName } returns "Full name"
        every { stars } returns "Stars: 55"
        every { language } returns "kotlin"
        every { avatarUrl } returns "some/url/filename.png"
        every { lastUpdate } returns "10 oct 2020"
    }
}