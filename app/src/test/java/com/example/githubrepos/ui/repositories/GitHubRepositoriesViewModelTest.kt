package com.example.githubrepos.ui.repositories

import com.example.githubrepos.data.model.domain.RepositoryDetails
import com.example.githubrepos.data.repository.GitHubRepositoriesRepository
import com.example.githubrepos.rule.CommonsRule
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.mockk
import io.mockk.verify
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.channelFlow
import kotlinx.coroutines.flow.emptyFlow
import kotlinx.coroutines.flow.flow
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class GitHubRepositoriesViewModelTest {
    @get:Rule
    val rule = CommonsRule(this)

    @MockK
    lateinit var gitHubRepositoriesRepository: GitHubRepositoriesRepository

    lateinit var viewModel: GitHubRepositoriesViewModel

    @Before
    fun setup() {
        every { gitHubRepositoriesRepository.getRepositories(any()) } returns emptyFlow()
        viewModel = GitHubRepositoriesViewModel(gitHubRepositoriesRepository)
    }

    @Test
    fun `get repositories called with first page`() {
        viewModel.getRepositories()
        verify {
            gitHubRepositoriesRepository.getRepositories(withArg {
                assertEquals("language:kotlin", it["q"])
                assertEquals("stars", it["sort"])
                assertEquals("desc", it["order"])
                assertEquals("1", it["page"])
                assertEquals("20", it["per_page"])
            })
        }
    }

    @Test
    fun `get repositories increments page gradually`() {
        val capturedParams = mutableListOf<Map<String, String>>()
        every { gitHubRepositoriesRepository.getRepositories(capture(capturedParams)) } returns emptyFlow()
        viewModel.getRepositories()
        viewModel.loadMore()
        viewModel.loadMore()
        verify(exactly = 3) { gitHubRepositoriesRepository.getRepositories(any()) }
        assertEquals("1", capturedParams[0]["page"])
        assertEquals("2", capturedParams[1]["page"])
        assertEquals("3", capturedParams[2]["page"])
    }

    @Test
    fun `should propagate response`() {
        val response = mutableListOf<RepositoryDetails>().apply {
            add(mockk())
            add(mockk())
            add(mockk())
        }
        every { gitHubRepositoriesRepository.getRepositories(any()) } returns flow { emit(response) }
        viewModel.getRepositories()
        assertEquals(response.size, viewModel.results.value?.size)
    }

    @Test
    fun `no results on error`() {
        every { gitHubRepositoriesRepository.getRepositories(any()) } returns flow {
            throw RuntimeException("e")
        }
        viewModel.getRepositories()
        assertEquals(null, viewModel.results.value)
    }

    @Test
    fun `should not request while loading`() {
        val haltedFlow = channelFlow<List<RepositoryDetails>> {
            awaitClose {}
        }
        every { gitHubRepositoriesRepository.getRepositories(any()) } returns haltedFlow

        viewModel.getRepositories()
        viewModel.loadMore()
        viewModel.loadMore()
        verify(exactly = 1) { gitHubRepositoriesRepository.getRepositories(match { it["page"] == "1" }) }
    }
}