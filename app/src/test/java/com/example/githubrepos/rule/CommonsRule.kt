package com.example.githubrepos.rule

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import io.mockk.MockKAnnotations
import org.junit.rules.RuleChain
import org.junit.rules.TestRule
import org.junit.runner.Description
import org.junit.runners.model.Statement

class CommonsRule(var classToRun: Any?) : TestRule {

    private val coroutineTestRule = CoroutineTestRule()
    private val instantTaskExecutorRule = InstantTaskExecutorRule()

    override fun apply(base: Statement?, description: Description?): Statement {
        return RuleChain
            .emptyRuleChain()
            .around(coroutineTestRule)
            .around(instantTaskExecutorRule)
            .around { _, _ ->
                object : Statement() {
                    override fun evaluate() {
                        classToRun?.let { MockKAnnotations.init(it) }
                        classToRun = null
                        base?.evaluate()
                    }

                }
            }
            .apply(base, description)

    }

}