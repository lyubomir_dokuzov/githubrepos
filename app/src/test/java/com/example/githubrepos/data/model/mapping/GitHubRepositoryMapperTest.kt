package com.example.githubrepos.data.model.mapping

import com.example.githubrepos.data.model.remote.GitHubRepository
import com.example.githubrepos.data.model.remote.Owner
import com.example.githubrepos.misc.Dispatcher
import com.example.githubrepos.misc.Dispatcherimpl
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import java.util.*

class GitHubRepositoryMapperTest {


    @MockK
    lateinit var dispatcher: Dispatcher
    private val testDispatcher = TestCoroutineDispatcher()

    lateinit var mapper: GitHubRepositoryMapper

    @Before
    fun setup() {
        MockKAnnotations.init(this)
        every { dispatcher.io() } returns testDispatcher
        mapper = GitHubRepositoryMapper(Dispatcherimpl())
    }

    @Test
    fun `ensure correct mapping`() = runBlocking {
        val input = GitHubRepository(
            "repoName",
            Owner("user", "url"),
            Date(1603790143973),
            "kotlin",
            2000L
        )
        val result = mapper.map(input)

        assertEquals("user/repoName", result.fullName)
        assertEquals("Stars: 2000", result.stars)
        assertEquals("Language: kotlin", result.language)
        assertEquals("url", result.avatarUrl)
        assertEquals("Last updated: 27 Oct 2020", result.lastUpdate)
    }

}